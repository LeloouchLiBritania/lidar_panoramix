var  parser, xmlDoc, test;

var imgs;

var url = "./Chapelle/listePanos.xml";

xmlDoc = fetch(url).then(
    function(response){
        return response.text().then(
            function(response){
                parser = new DOMParser();
                return parser.parseFromString(response,"text/xml");
            }
        )
    }
)

/**
 * Returns a promise with the position of an intervisible pano in the system of the actual pano.
 * Returns Promise<{x: 0, y:0, z:0}> if the two panos are not intervisible.
 * @param {Number} startPanoRang - ID of the actual pano (starting pano)
 * @param {Number} endPanoRang - ID of the futur pano (destination pano)
 * @returns {Promise<{x: Number, y: Number, z: Number}>} Position of the destination pano in a promess. 
 */
function getPanoPosition(startPanoRang, endPanoRang){
    return xmlDoc.then(function(response){
        
        let panoPosition = {x: 0, y: 0, z: 0};
        
        let pano = response.getElementsByTagName("pano")[startPanoRang];
        let v0  = parseFloat(pano.getElementsByTagName("v0")[0].childNodes[0].nodeValue)*Math.PI/180;
        let panos2D = pano.getElementsByTagName("pano2D");

        for (i = 0; i < panos2D.length; i++){
            let rang = parseInt(panos2D[i].getElementsByTagName("rang")[0].childNodes[0].nodeValue);
            if (rang == endPanoRang){
                let phi = - parseFloat(panos2D[i].getElementsByTagName("x")[0].childNodes[0].nodeValue);
                let theta = parseFloat(panos2D[i].getElementsByTagName("y")[0].childNodes[0].nodeValue);
                phi = phi*Math.PI/2880 - Math.PI / 2 - v0;
                theta = theta*Math.PI/2880;
                let x = Math.sin(theta) * Math.cos(phi);
                let y = Math.sin(theta) * Math.sin(phi);
                let z = Math.cos(theta);
                panoPosition = {x: y*4, y: z*4, z: x*4};
                break;
            }
        }
        console.log(panoPosition);
        return panoPosition;
    })
}

/**
 * Returns a promise with the information of a pano from the XML description file.
 * @param {Number} rangPano - ID of the pano
 * @returns {Promise<{img: String, v0: Number}>} Information of the pano. 
 */
function getPanoInfo(rangPano){
    return xmlDoc.then(function(response){
        let pano = response.getElementsByTagName("pano")[rangPano];
        let img = pano.getElementsByTagName("name")[0].childNodes[0].nodeValue.replace(" ", "");
        let v0  = parseFloat(pano.getElementsByTagName("v0")[0].childNodes[0].nodeValue)*Math.PI/180;
        let panoInfo = {img: img, v0: v0};
        console.log("Info pano: ", panoInfo);
        return panoInfo;
    })
}

/**
 * Returns a promise with the information of all the panos visible from a pano from the XML description file.
 * @param {Number} rangPano - ID of the pano
 * @returns {Promise<[{id: Number, x: Number, y: Number, z: Number}]>} Information of all the panos visible from a pano. 
 */
function getOtherPanos(rangPano){
    return xmlDoc.then(function(response){

        let pano = response.getElementsByTagName("pano")[rangPano];
        let v0  = parseFloat(pano.getElementsByTagName("v0")[0].childNodes[0].nodeValue)*Math.PI/180;
        let panos2D = pano.getElementsByTagName("pano2D");

        let otherPanos = [];

        for (i = 0; i < panos2D.length; i++){
            let rang = parseInt(panos2D[i].getElementsByTagName("rang")[0].childNodes[0].nodeValue);
            let phi = - parseFloat(panos2D[i].getElementsByTagName("x")[0].childNodes[0].nodeValue);
            let theta = parseFloat(panos2D[i].getElementsByTagName("y")[0].childNodes[0].nodeValue);
            phi = phi*Math.PI/2880 - Math.PI / 2 - v0;
            theta = theta*Math.PI/2880;
            let x = Math.sin(theta) * Math.cos(phi);
            let y = Math.sin(theta) * Math.sin(phi);
            let z = Math.cos(theta);
            otherPanos.push({id: rang, x: x, y: y, z: z});
        }
        console.log("Info other panos:", otherPanos);
        return otherPanos;
    })
}

/**
 * Returns a promise with the information of all the details visible from a pano from the XML description file.
 * @param {Number} rangPano - ID of the pano
 * @returns {Promise<[{id: Number, x: Number, y: Number, z: Number}]>} Information of all the details visible from a pano. 
 */
function getDetails(rangPano){
    return xmlDoc.then(function(response){

        let pano = response.getElementsByTagName("pano")[rangPano];
        let v0  = parseFloat(pano.getElementsByTagName("v0")[0].childNodes[0].nodeValue)*Math.PI/180;
        let details2D = pano.getElementsByTagName("detail2D");
    
        let details = [];
    
        for (i = 0; i < details2D.length; i++){
            let titre = details2D[i].getElementsByTagName("titre")[0].childNodes[0].nodeValue;
            let url = details2D[i].getElementsByTagName("url")[0].childNodes[0].nodeValue;
            let phi = - parseFloat(details2D[i].getElementsByTagName("x")[0].childNodes[0].nodeValue);
            let theta = parseFloat(details2D[i].getElementsByTagName("y")[0].childNodes[0].nodeValue);
            phi = phi*Math.PI/2880 - Math.PI / 2 - v0;
            theta = theta*Math.PI/2880;
            let x = Math.sin(theta) * Math.cos(phi);
            let y = Math.sin(theta) * Math.sin(phi);
            let z = Math.cos(theta);
            details.push({titre: titre, url: url, x: x, y: y, z: z});
        }
        console.log("Info details:", details);
        return details;
    })
        
}